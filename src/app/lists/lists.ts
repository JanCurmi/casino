import { GameListComponent } from "./games/game-list.component"
import {GameThumbComponent} from "./game-thumb/game-thumb.component"
export const ListComponents = [
    GameListComponent,
    GameThumbComponent
]