import { Component, Input, OnInit , ChangeDetectionStrategy } from "@angular/core";
import { Game, GameMockClient } from "src/app/shared";
import { MatTableDataSource } from "@angular/material/table";

@Component({
selector: "app-game-list",
templateUrl: "./game-list.component.html",
styleUrls: ["./game-list.component.scss"],
changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameListComponent implements OnInit {
gamesData$: Game[] = [];
dataSource = new MatTableDataSource<Game>();
displayedColumns = ["id", "title", "provider", "tag"];

constructor(private gameMockClient: GameMockClient) {}
@Input() set filterValue(value: string | null) {
if (!value) { return; }
this.dataSource.filter = value;
}

ngOnInit(): void { this.dataSource = new MatTableDataSource(this.getTrendingGames()); }

onFilterChanged(value: string) { this.filterValue = value; }

private fetchRows() : Game[] {
let trendingGames : Game [] = [];
this.gameMockClient.getAll$().subscribe(response => {
trendingGames = response.filter(x => x.tag?.toLowerCase().trim() === "trending");
this.saveToLocale(response);
}, ex => { console.log(ex); });
return trendingGames;
}

private getTrendingGames() : Game [] {
return this.gameMockClient.localDefaults?.games.length ? this.gameMockClient.localDefaults.games.filter(x => x.tag?.toLowerCase().trim() === "trending") : this.fetchRows();
}

private saveToLocale(casinoGames: Game []) {
const localDefaults = this.gameMockClient.localDefaults;
localDefaults.games = casinoGames;
this.gameMockClient.localDefaults = localDefaults;
}
}
