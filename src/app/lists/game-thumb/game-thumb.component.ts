import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { Game, GameMockClient } from "src/app/shared";

@Component({
selector: "app-game-thumb",
templateUrl: "./game-thumb.component.html",
styleUrls: ["./game-thumb.component.scss"]
})
export class GameThumbComponent implements OnInit {
dataSource = new MatTableDataSource<Game>();
displayedColumns = ["image", "title", "button"];
ignoreRowClick = false;
gameModel: Game | undefined;
gamePicture = "";
providersList: string [] = [];
@Output() providers: EventEmitter<string[]> = new EventEmitter<string[]>();

constructor(private router: Router, private gameMockClient: GameMockClient) { }

@Input() set filterValue(value: string) {
this.dataSource.filter = value;
this.router.navigate([], { queryParams: {provider: value.trim()}});
}

@Input() set onSearch(games : Game [] | undefined) {
if (!games) { return; }
this.dataSource.data = games.filter( x => x.thumb);
}
ngOnInit(): void { this.getGameThumbs(); }

onPlayGame(game : Game) { this.saveToLocale(undefined, game); window.open(game.startUrl, "_blank"); }

private fetchRows(){
let gameThumbs : Game [] = [];
this.gameMockClient.getAll$().subscribe(response => {
gameThumbs = response.filter( x => x.thumb);
this.dataSource = new MatTableDataSource(gameThumbs);
this.saveToLocale(response);
}, ex => { console.log(ex); });
return gameThumbs;
}

private getGameThumbs() {
const gameThumbs = this.gameMockClient.localDefaults?.games?.length ?
this.gameMockClient.localDefaults.games.filter( x => x.thumb) : this.fetchRows();
this.dataSource = new MatTableDataSource(gameThumbs);
this.setProviders(gameThumbs);
}

private saveToLocale(casinoGames?: Game [], playedGame?: Game) {
const localDefaults = this.gameMockClient.localDefaults;
if (casinoGames) {localDefaults.games = casinoGames; }
if (playedGame) {localDefaults.lastPlayed.push(playedGame); }
this.gameMockClient.localDefaults = localDefaults;
}

private setProviders(array : Game[]) {
this.providers.emit(array.filter((v, i , a) => a.findIndex(t => ( t.providerName === v.providerName )) === i ).map( x => x.providerName ));
}
}
