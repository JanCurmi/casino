import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//App
import { AppRoutingModule, PageComponents } from './app-routing.module';
import { AppComponent } from "./app.component";

//Components
import { ListComponents } from "./lists/lists";
import { ThemeComponents } from './theme/theme';
import { ElementComponents } from './elements/elements';
//Material
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';

//Services
import { LoadingService } from 'src/services/loading.service';
import { SidenavService } from 'src/services/sidenav.service';


@NgModule({
	declarations: [
		AppComponent,
		PageComponents,
		ListComponents,
		ThemeComponents,
		ElementComponents
	],
	imports: [
		AppRoutingModule,
		BrowserAnimationsModule,
		BrowserModule,
		CommonModule,
		FormsModule,
		HttpClientModule,
		ReactiveFormsModule,
		RouterModule,

		MatAutocompleteModule,
		MatButtonModule,
		MatButtonToggleModule,
		MatCardModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatListModule,
		MatMenuModule,
		MatProgressSpinnerModule,
		MatSidenavModule,
		MatSortModule,
		MatTableModule,
		MatToolbarModule
	],
	providers: [
		DatePipe,
		LoadingService,
		SidenavService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
