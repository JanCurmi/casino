import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SidenavService } from 'src/services/sidenav.service';

@Component({
  selector: 'ctl-nav-list',
  templateUrl: './nav-list.component.html',
  styleUrls: ['./nav-list.component.scss']
})
export class NavListComponent implements OnInit {
  constructor(private router: Router,private sidenav: SidenavService) { }
  
  onToggleMenu() {
    this.sidenav.toggle();
  }

  ngOnInit(): void {
  }

}
