import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SidenavService } from 'src/services/sidenav.service';

@Component({
  selector: 'ctl-web',
  templateUrl: './web-template.component.html',
  styleUrls: ['./web-template.component.scss']
})
export class WebTemplateComponent implements OnInit {
  constructor(private sidenavService: SidenavService) { }
  
  @ViewChild('snav')
  public sidenav!: MatSidenav;

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.sidenavService.setSidenav(this.sidenav);
  }
}
