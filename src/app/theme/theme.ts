import { WebTemplateComponent } from './web-template/web-template.component';
import { NavListComponent } from './nav-list/nav-list.component';
import {ToolbarComponent} from './toolbar/toolbar.component';

export const ThemeComponents = [
    WebTemplateComponent,
    NavListComponent,
    ToolbarComponent
];
