import { Component, OnInit } from '@angular/core';
import { SidenavService } from 'src/services/sidenav.service';

@Component({
  selector: 'ctl-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  constructor(private sidenav: SidenavService) { }

  public onToggleMenu() {
    this.sidenav.toggle();
  }
  
  ngOnInit(): void {
  }

}
