import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Observable } from "rxjs";
import { Game, GameMockClient } from "src/app/shared";
import { debounceTime, startWith, switchMap } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
selector: "ctl-search",
templateUrl: "./search.component.html",
styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
form: FormGroup | undefined;
games: Game[] = [];
filteredGames: Observable<Game[] | undefined> | undefined;
isSearching = false;
@Output() gameSearch: EventEmitter<Game[] | undefined> = new EventEmitter<Game[] | undefined>();

constructor(private formBuilder: FormBuilder, private gameMockClient: GameMockClient,
private router: Router, private httpClient: HttpClient) { }

ngOnInit(): void { this.createFormGroup(); }

filterGames() {
this.filteredGames = this.form?.get("gameName")!.valueChanges.pipe( startWith(""), debounceTime(500),
switchMap(value => {
const filterValue: string = value.toLowerCase();
return new Observable<Game[]>((s) => {
const result = this.games.filter(x => x.title.toLowerCase().indexOf( filterValue.trim()) === 0);
s.next(result);
this.router.navigate([], { queryParams: {searchTerm: filterValue.trim().toLowerCase() }});
this.gameSearch.emit(result); }); }), ); }

private createFormGroup() {
this.form = this.formBuilder.group({  gameName: new FormControl("", [Validators.required]) });
this.fetchRows();
this.filterGames();
}

private fetchRows() {
this.gameMockClient.getAll$().subscribe(response => {
this.games = response;
}, ex => { });
}
}
