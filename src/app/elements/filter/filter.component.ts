import { Component, EventEmitter, Output } from "@angular/core";

@Component({
selector: "ctl-filter",
templateUrl: "./filter.component.html",
styleUrls: ["./filter.component.css"]
})
export class FilterComponent {
showInput = false;
filterValue = "";
@Output() changed: EventEmitter<string | null> = new EventEmitter<string | null>();

applyFilter() {
const filter = this.filterValue ? this.filterValue.toString().trim().toLowerCase() : "";
this.changed.emit(filter);
}

onClear() {  this.filterValue = ""; this.changed.emit(null); }
onToggleInput(value: boolean) { this.showInput = value; }

}
