import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ctl-spinner-load',
  templateUrl: './spinner-element.component.html',
  styleUrls: ['./spinner-element.component.css']
})
export class SpinnerElementComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
