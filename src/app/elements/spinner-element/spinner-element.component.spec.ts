import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerElementComponent } from './spinner-element.component';

describe('OverlaySpinnerLoadComponent', () => {
  let component: SpinnerElementComponent;
  let fixture: ComponentFixture<SpinnerElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpinnerElementComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
