import { FilterComponent } from "./filter/filter.component"
import {SearchComponent} from "./search/search.component"
import { SpinnerElementComponent } from "./spinner-element/spinner-element.component"
import { ProviderPickerComponent } from "./provider-picker/provider-picker.component"


export const ElementComponents = [
    FilterComponent,
    SearchComponent,
    SpinnerElementComponent,
    ProviderPickerComponent
]