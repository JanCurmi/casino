import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
selector: "ctl-provider-picker",
templateUrl: "./provider-picker.component.html",
styleUrls: ["./provider-picker.component.scss"]
})
export class ProviderPickerComponent{
providerList: string[] = [];

@Input() set providers(val: string []) { if (!val) { return; } this.providerList = val; }
@Output() selectedProvider: EventEmitter<string> = new EventEmitter<string>();

onFilterByProvider(value: string) { this.selectedProvider.emit(value); }

}
