import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ProviderPickerComponent } from "./provider-picker.component";

describe("ProviderPickerComponent", () => {
let component: ProviderPickerComponent;
let fixture: ComponentFixture<ProviderPickerComponent>;

beforeEach(async () => {
await TestBed.configureTestingModule({
declarations: [ ProviderPickerComponent ]
})
.compileComponents();
});

beforeEach(() => {
fixture = TestBed.createComponent(ProviderPickerComponent);
component = fixture.componentInstance;
fixture.detectChanges();
});

it("should create", () => {
expect(component).toBeTruthy();
});
});
