import { Component} from "@angular/core";
import { Game } from "src/app/shared";

@Component({
selector: "app-games",
templateUrl: "./games.component.html",
styleUrls: ["./games.component.scss"]
})
export class GamesComponent {
searchGame = false;
providers : string [] = [];
games : Game[] | undefined = [];
providerSelected = "";

onProviderList(model: string[]) { this.providers = model; }
onFilterSearch(games: Game[] | undefined) { this.games = games; }
onFilterByProvider(provider: any) { this.providerSelected = provider; }
}
