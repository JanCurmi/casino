import { Component, ChangeDetectionStrategy } from "@angular/core";
const NAME_KEBAB = "app-home";

@Component({
	templateUrl: "./home.component.html",
	styleUrls: ["./home.scss"],
	host: { class: NAME_KEBAB },
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
filterValue : string | null = "";
onFilterChanged(value: string | null) { this.filterValue = value; }
}
