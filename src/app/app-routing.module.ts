import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { HomeComponent } from "./pages/home/home.component";
import { GamesComponent } from "./pages/games/games.component";

const routes: Routes = [
	{ path: "games", component: GamesComponent },
	{ path: 'home', component: HomeComponent },
	{ path: "", component: HomeComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes, { useHash: true })],
	exports: [RouterModule]
})

export class AppRoutingModule { }

export const PageComponents = [
	HomeComponent,
	GamesComponent,
];
