import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Game } from "./game.model";

@Injectable({providedIn: "root"})
export class GameMockClient {
private readonly dataURL = "assets/game.mock-data.json";
private _localDefaults: LocalDefaults | undefined;

constructor(private http: HttpClient) {	}
getAll$(): Observable<Game[]> {	return this.http.get<Game[]>(this.dataURL); }

get localDefaults(): LocalDefaults  {
try {
if (!this._localDefaults) { const item = localStorage.getItem("localDefaults"); if (!item) { return new LocalDefaults() ; } 
this._localDefaults = JSON.parse(item);
}
return this._localDefaults ?? new LocalDefaults;
} catch (error) {
localStorage.removeItem("localDefaults");
return new LocalDefaults();
}}

set localDefaults(v: LocalDefaults) {
try { this._localDefaults = v;
if (!v) { localStorage.removeItem("localDefaults"); } else { const item = JSON.stringify(v);localStorage.setItem("localDefaults", item); }
} catch (error) { localStorage.removeItem("localDefaults"); }
}}

export class LocalDefaults {
	games: Game [] = [];
	lastPlayed: Game [] = [];
}