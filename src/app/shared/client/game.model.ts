export interface Game {
	id: number;
	slug: string;
	title: string;
	providerName: string;
	tag: string;
	thumb: GameThumb;
	startUrl: string;
}


export interface GameThumb {
	title: string;
	url: string;
}
